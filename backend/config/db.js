const mongoose = require("mongoose");

const connectDB = () => {
  mongoose.set("strictQuery", true);
  mongoose
    .connect(process.env.MONGO_URI, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    })
    .then(() => {
      console.log(`MongoDB Connected.`);
    })
    .catch((error) => {
      console.error(`Mongoose Error: ${error.message}`);
      process.exit(1);
    });

  mongoose.connection.on("disconnected", () => {
    console.log("Mongoose default connection disconnected");
  });
};

module.exports = connectDB;
