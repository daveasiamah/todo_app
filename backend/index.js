const express = require("express");
const morgan = require("morgan");
const app = express();
const dotenv = require("dotenv");
const todoRoutes = require("./routes/todoRoutes.js");
const { notFound, errorHandler } = require("./middleware/errorMiddleware.js");
const connectDB = require("./config/db.js");

dotenv.config();

app.use(express.json());
app.use(morgan("dev"));
// app.use(notFound);
// app.use(errorHandler);

app.use("/api/todos", todoRoutes);
console.log(process.env.MONGO_URI);
connectDB();
const PORT = process.env.PORT || 5000;
app.listen(PORT, console.log(`Server running on port: ${PORT}`));
