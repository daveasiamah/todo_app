const mongoose = require("mongoose");

const subTaskSchema = mongoose.Schema(
  {
    id: { type: mongoose.Schema.Types.ObjectId },
    title: { type: String, required: true },
    status: { type: String, default: "pending" },
    todo: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Todo",
    },
  },
  {
    timestamps: true,
  }
);

const SubTask = mongoose.model("SubTask", subTaskSchema);
module.exports = SubTask;
