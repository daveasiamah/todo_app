// import asyncHandler from "express-async-handler";
const Todo = require("../models/Todo.js");
const SubTask = require("../models/SubTask");

// @desc    Fetch all todos
// @access  Public
const getTodos = async (req, res, next) => {
  try {
    const todos = await Todo.aggregate([
      {
        $lookup: {
          from: "subtasks",
          localField: "_id",
          foreignField: "todo",
          as: "subtasks",
        },
      },
      { $sort: { createdAt: -1 } },
    ]);
    res.status(200).json(todos);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
};

// @desc    Fetch single todo
// @route   GET /api/todos/:id
// @access  Public
const getTodoById = async (req, res, next) => {
  const todo = await todo.findById(req.params.id);

  if (todo) {
    res.json(todo);
  } else {
    res.status(404);
    throw new Error("Todo not found");
  }
};

// @desc    Delete a todo
// @route   DELETE /api/todos/:id
const deleteTodo = async (req, res) => {
  try {
    const todo = await Todo.findById(req.params.id);
    if (!todo) {
      return res.status(404).json({ message: "Todo not found" });
    }
    await todo.remove();
    res.status(200).json({ message: "Todo removed" });
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
};

// @desc    Create a todo
// @route   POST /api/todos
const createTodo = async (req, res, next) => {
  const { title, status, subtasks } = req.body;

  try {
    const todo = new Todo({ title, status });
    if (subtasks) {
      const newSubTasks = subtasks.map((subtask) => {
        return new SubTask({ title: subtask.title, todo: todo._id });
      });
      const savedSubTasks = await SubTask.insertMany(newSubTasks);
      todo.subtasks = savedSubTasks.map((subtask) => subtask._id);
    }
    const savedTodo = await todo.save();
    const populatedTodo = await Todo.findById(savedTodo._id).populate("subtasks");
    res.status(201).json(populatedTodo);
  } catch (error) {
    res.status(500).json({ message: "Error saving todo", data: error.toString() });
  }
};

// @desc    Update a todo
// @route   PUT /api/todos/:id
const updateTodo = async (req, res, next) => {
  const todoRes = await db.collection("todos").doc("todo").update(
    {
      title,
      status: "pending",
      created_at: Date.now().toString(),
    },
    { merge: true }
  );

  //const createdTodo = await todo.save();
  res.status(201).json({ message: todoRes });
};

module.exports = { getTodos, getTodoById, deleteTodo, createTodo, updateTodo };
