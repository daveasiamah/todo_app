const express = require("express");
const router = express.Router();
const {
  getTodos,
  getTodoById,
  deleteTodo,
  createTodo,
  updateTodo,
} = require("../controllers/todoController.js");

router.route("/").get(getTodos).post(createTodo);
router.route("/:id").get(getTodoById).delete(deleteTodo).put(updateTodo);

module.exports = router;
